import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormularioDestinoViajeComponent } from './formulario-destino-viaje/formulario-destino-viaje.component';
import { DestinosViajesState, reducersDestinosViajes, initializeDestinosViajeState, DestinoViajesEffects} from './models/destinos-viajes-state.model'
import { ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Inicio de declaración redux
export interface AppState {
  destinos: DestinosViajesState;
}
const reducers: ActionReducerMap<AppState> = {
  destinos: reducersDestinosViajes
};

// aca esta la inicialización
const reducersEstadoInicial = {
  destinos: initializeDestinosViajeState()
}
// Fin de declaración redux

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormularioDestinoViajeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, {initialState: reducersEstadoInicial}),
    EffectsModule.forRoot([DestinoViajesEffects])
  ],
  providers: [], // aca van las inyecciones de dependencias de los servicioss
  bootstrap: [AppComponent]
})
export class AppModule { }
