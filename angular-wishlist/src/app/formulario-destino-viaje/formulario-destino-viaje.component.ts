import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import {map, filter, debounceTime, distinctUntilChanged ,switchMap} from 'rxjs/operators';
import {ajax, AjaxResponse} from 'rxjs/ajax';

@Component({
  selector: 'app-formulario-destino-viaje',
  templateUrl: './formulario-destino-viaje.component.html',
  styleUrls: ['./formulario-destino-viaje.component.css']
})
export class FormularioDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  
  minLongitud: number =5;
  searchResults : string[];

  datosFormulario= this.formBuilder.group({
    nombre:['', [Validators.required, this.nombreValidadorParametrizado(this.minLongitud)]],
    url:['']
  });

  constructor(private formBuilder: FormBuilder) {    
    this.onItemAdded = new EventEmitter();

    this.datosFormulario.valueChanges.subscribe( (form: any)=>{
      console.log('hay un cambio:', form);
    });
  }

  ngOnInit(): void {
    let elemNombre= <HTMLInputElement>document.getElementById('nombre');
    //una forma de hacerlo
    this.datosFormulario.controls['nombre'].valueChanges.subscribe( (nombre)=>{
      console.log("cambio"+ nombre);
    });
    // segunda forma con pipe es mejor y se puede conectar a un servicio para buscar información
    fromEvent(elemNombre, 'input').pipe(
      map( (e: KeyboardEvent)=>(e.target as HTMLInputElement).value),
      filter(text => text.length >2 ),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap( ()=> ajax('/assets/datos.json'))
    ).subscribe( ajaxResponse => {
      this.searchResults= ajaxResponse.response;
      console.log("resultados busqueda: "+this.searchResults);
    })
  }

  
  guardar(){
    //console.log("1", this.datosFormulario.value.nombre);
    //console.log("2", this.datosFormulario.get('nombre').value);
    //console.log("3", this.datosFormulario.controls['nombre'].value);
    let destino= new DestinoViaje(this.datosFormulario.value.nombre, this.datosFormulario.value.url);
    this.onItemAdded.emit(destino);
    console.log(destino);
  }

  /*
  guardar(nombre: string, url : string){

    let destino= new DestinoViaje(nombre, url);
    this.onItemAdded.emit(destino);
    console.log(destino);
  }
  */

  nombreValidador(control: FormControl):{[s:string]:boolean}{
    let nombre = control.value.toString();
    //onsole.log(this.minLongitud);
    if(nombre.trim().length < 5){
      return {nombreInvalido: true};
    }
    return null;
  }

  nombreValidadorParametrizado(minLongitud: number): ValidatorFn{
    return (control:FormControl): {[s: string]:boolean} |null =>{
      let nombre = control.value.toString();
      //console.log(this.minLongitud);
      if(nombre.trim().length < 5){
        return {longNombre: true};
      }
      return null;
    }
  } 

}
