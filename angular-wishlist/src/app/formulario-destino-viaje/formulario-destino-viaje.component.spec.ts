import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioDestinoViajeComponent } from './formulario-destino-viaje.component';

describe('FormularioDestinoViajeComponent', () => {
  let component: FormularioDestinoViajeComponent;
  let fixture: ComponentFixture<FormularioDestinoViajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioDestinoViajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioDestinoViajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
