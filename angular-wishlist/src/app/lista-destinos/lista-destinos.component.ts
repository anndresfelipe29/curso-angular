import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {DestinoViaje} from '../models/destino-viaje.model';
import {DestinoApiClient} from '../models/destinos-api-client.model';
import { Store, State } from '@ngrx/store';
import { AppState } from '../app.module';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  // @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];



  constructor(public destinosApiClient: DestinoApiClient, private store: Store<AppState>) {
    // this.onItemAdded = new EventEmitter();
    // this.destinos= [];
    this.updates = [];
    /*this.store.select(state => state.destinos.favorito)
    .subscribe( data => {
      if (data != null){
        console.log(data.nombre + ' se agrego a updates ', this.updates);
        this.updates.push('Se a elegido a: ' + data.nombre);
      }

    });*/
    // Aca se llama al favorito de manera normal y es cambiado por el codigo de arriba que usa el store
    this.destinosApiClient.subscribedOnChange( (destino: DestinoViaje) => {
      if (destino != null){
        console.log(destino.nombre + ' se agrego a updates ', this.updates);
        this.updates.push('Se a elegido a: ' + destino.nombre);
      }
    });
  }

  ngOnInit(): void {
  }

  agregado(destino: DestinoViaje){
    this.destinosApiClient.add(destino);
    // this.onItemAdded.emit(destino);
  }

  // guardar es reemplazado por agregar, no se utiliza
 /* guardar(nombre: HTMLInputElement, url:HTMLInputElement):boolean{
    this.nombre= nombre.toString();
    this.url= url.toString();
    this.destinos.push(new DestinoViaje(this.nombre, this.url) ) ;
    console.log(this.destinos);
    return false;
  }
  */

  elegido(destino: DestinoViaje){
    this.destinosApiClient.elegir(destino);
  }

}
