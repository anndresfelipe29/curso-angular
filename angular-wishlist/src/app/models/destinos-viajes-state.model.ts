// import {} from
import { DestinoViaje } from '../models/destino-viaje.model';
import { Action } from '@ngrx/store';
import { ofType, Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


// Redux busca manejar un unico estado donde se almacena toda la información de la aplicación 

// Estado
export interface DestinosViajesState{
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;

}

export const initializeDestinosViajeState = function(){
    return {
        items: [],
        loading: false,
        favorito: null
    };
}

// Acciones
export enum DestinosViajesActionTypes{
    NUEVO_DESTINO = '[Destinos] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito'
}

export class NuevoDestinoAction implements Action{
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje){}
}

export class ElegidoFavoritoAction implements Action{
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje){}
}

export type DestinosViajesAction = NuevoDestinoAction | ElegidoFavoritoAction;

// Reducers

export function reducersDestinosViajes(
    state: DestinosViajesState,  // estado anterior del sistema
    action: DestinosViajesAction // Cambio que se ejecuta
): DestinosViajesState{
    switch (action.type){
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
    }
    return state;
}

// Effects
@Injectable()
export class DestinoViajesEffects{
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),  // ofType si es de este tipo entonces se hace lo siguiente
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions){}
}


