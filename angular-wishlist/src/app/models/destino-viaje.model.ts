export class DestinoViaje {
    //nombre: string;
    //url: string;
    private selected: boolean;
    public servicios: string[];

    constructor( public nombre:string ,public url:string){
        //con el public en el constructor ya no se requiere declarar las variables antes
        this.servicios= ['Desayuno', 'Almuerzo'];
    }

    isSelected(): boolean{
        return this.selected;
    }

    setSelected(valor: boolean){
        this.selected=valor;
    }
}